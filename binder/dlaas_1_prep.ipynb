{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# SKAO SDC1 Workflow on DLaaS: Preparation\n",
    "\n",
    "This notebook performs the preparation steps for running the SDC1 workflow.\n",
    "\n",
    "The data files must be staged and assigned variables in the Rucio tab. We then create symlinks to the data in this environment. Then we use several Python wrapper methods to apply the primary-beam correction to the images, and separate out the training area for later classification.\n",
    "\n",
    "After performing the steps in this notebook, the source finding routine can be performed on the PB-corrected images. This involves running the script 2_source_find.py.\n",
    "\n",
    "## Definitions"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "import os\n",
    "from pathlib import Path\n",
    "from time import time\n",
    "\n",
    "import numpy as np\n",
    "from ska_sdc import Sdc1Scorer\n",
    "from sklearn.ensemble import RandomForestClassifier\n",
    "from sklearn.metrics import classification_report\n",
    "\n",
    "from ska.sdc1.models.sdc1_image import Sdc1Image\n",
    "from ska.sdc1.utils.bdsf_utils import cat_df_from_srl_df, load_truth_df\n",
    "from ska.sdc1.utils.classification import SKLClassification\n",
    "from ska.sdc1.utils.source_finder import SourceFinder\n",
    "\n",
    "# Challenge frequency bands\n",
    "#\n",
    "FREQS = [560, 1400, 9200]\n",
    "\n",
    "\n",
    "# Input data paths\n",
    "#\n",
    "def image_path(freq):\n",
    "    return os.path.join(\"data\", \"sample_images\", \"{}mhz_1000h.fits\".format(freq))\n",
    "\n",
    "\n",
    "def pb_path(freq):\n",
    "    return os.path.join(\"data\", \"sample_images\", \"{}mhz_pb.fits\".format(freq))\n",
    "\n",
    "\n",
    "def train_truth_path(freq):\n",
    "    return os.path.join(\"data\", \"truth\", \"{}mhz_truth_train.txt\".format(freq))\n",
    "\n",
    "\n",
    "def full_truth_path(freq):\n",
    "    return os.path.join(\"data\", \"truth\", \"{}mhz_truth_full.txt\".format(freq))\n",
    "\n",
    "\n",
    "# Output data paths\n",
    "#\n",
    "def train_source_df_path(freq):\n",
    "    return os.path.join(\"data\", \"sources\", \"{}mhz_sources_train.csv\".format(freq))\n",
    "\n",
    "\n",
    "def full_source_df_path(freq):\n",
    "    return os.path.join(\"data\", \"sources\", \"{}mhz_sources_full.csv\".format(freq))\n",
    "\n",
    "\n",
    "def submission_df_path(freq):\n",
    "    return os.path.join(\"data\", \"sources\", \"{}mhz_submission.csv\".format(freq))\n",
    "\n",
    "\n",
    "def model_path(freq):\n",
    "    return os.path.join(\"data\", \"models\", \"{}mhz_classifier.pickle\".format(freq))\n",
    "\n",
    "\n",
    "def score_report_path(freq):\n",
    "    return os.path.join(\"data\", \"score\", \"{}mhz_score.txt\".format(freq))\n",
    "\n",
    "\n",
    "def write_df_to_disk(df, out_path):\n",
    "    \"\"\" Helper function to write DataFrame df to a file at out_path\"\"\"\n",
    "    out_dir = os.path.dirname(out_path)\n",
    "    Path(out_dir).mkdir(parents=True, exist_ok=True)\n",
    "    df.to_csv(out_path, index=False)"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Create symlinks to data in notebook directory\n",
    "\n",
    "This allows the workflow code to operate in this environment without physically copying the source data across."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "sample_data_dir = os.path.join('data', 'sample_images')\n",
    "Path(sample_data_dir).mkdir(parents=True, exist_ok=True)\n",
    "truth_dir = os.path.join('data', 'truth')\n",
    "Path(truth_dir).mkdir(parents=True, exist_ok=True)\n",
    "\n",
    "\n",
    "sample_paths_dl = {560: sample_560, 1400: sample_1400, 9200: sample_9200}\n",
    "pb_path_dl = {560: pb_560, 1400: pb_1400, 9200: pb_9200}\n",
    "truth_train_path_dl = {560: truth_train_560, 1400: truth_train_1400, 9200: truth_train_9200}\n",
    "truth_full_path_dl = {560: truth_full_560, 1400: truth_full_1400, 9200: truth_full_9200}\n",
    "\n",
    "\n",
    "for freq in FREQS:\n",
    "    sample_path_sym = os.path.join(sample_data_dir, \"{}mhz_1000h.fits\".format(freq))\n",
    "    pb_path_sym = os.path.join(sample_data_dir, \"{}mhz_pb.fits\".format(freq))\n",
    "    truth_train_path_sym = os.path.join(truth_dir, \"{}mhz_truth_train.txt\".format(freq))\n",
    "    truth_full_path_sym = os.path.join(truth_dir, \"{}mhz_truth_full.txt\".format(freq))\n",
    "    os.symlink(sample_paths_dl[freq], sample_path_sym)\n",
    "    os.symlink(pb_path_dl[freq], pb_path_sym)\n",
    "    os.symlink(truth_train_path_dl[freq], truth_train_path_sym)\n",
    "    os.symlink(truth_full_path_dl[freq], truth_full_path_sym)"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Preprocess images (PB correction) and crop out the training area for building ML model"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "print(\"Starting preprocessing\")\n",
    "time_0 = time()\n",
    "sdc1_image_list = []\n",
    "for freq in FREQS:\n",
    "    print(\"Preprocessing {}MHz image\".format(freq))\n",
    "    new_image = Sdc1Image(freq, image_path(freq), pb_path(freq))\n",
    "    new_image.preprocess()\n",
    "    sdc1_image_list.append(new_image)\n",
    "print(\"Preprocessing complete in {:.2f}s\".format(time() - time_0))"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "In data/sample_images, we now have PB-corrected and training images for each band"
   ],
   "metadata": {}
  }
 ],
 "metadata": {
  "orig_nbformat": 4,
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}