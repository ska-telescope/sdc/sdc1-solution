import os
from pathlib import Path
from time import time

from ska.sdc1.utils.source_finder import SourceFinder

train_images = {
    560: "data/sample_images/560mhz_1000h_train.fits",
    1400: "data/sample_images/1400mhz_1000h_train.fits",
    9200: "data/sample_images/9200mhz_1000h_train.fits",
}
pbcor_images = {
    560: "data/sample_images/560mhz_1000h_pbcor.fits",
    1400: "data/sample_images/1400mhz_1000h_pbcor.fits",
    9200: "data/sample_images/9200mhz_1000h_pbcor.fits",
}

# Output data paths
#
def train_source_df_path(freq):
    return os.path.join("data", "sources", "{}mhz_sources_train.csv".format(freq))


def full_source_df_path(freq):
    return os.path.join("data", "sources", "{}mhz_sources_full.csv".format(freq))


def write_df_to_disk(df, out_path):
    """ Helper function to write DataFrame df to a file at out_path"""
    out_dir = os.path.dirname(out_path)
    Path(out_dir).mkdir(parents=True, exist_ok=True)
    df.to_csv(out_path, index=False)


if __name__ == "__main__":
    time_0 = time()
    # 1) Source finding (training):
    print("Source finding (train)...")
    sources_training = {}
    for freq, train_image in train_images.items():
        source_finder = SourceFinder(train_image)
        sl_train_df = source_finder.run()
        sources_training[freq] = sl_train_df

        # (Optional) Write source list DataFrame to disk
        write_df_to_disk(sl_train_df, train_source_df_path(freq))

        # Remove temp files:
        source_finder.reset()

    # 2) Source finding (full):
    sources_full = {}
    print("Source finding (full)...")
    for freq, pbcor_image in pbcor_images.items():
        source_finder = SourceFinder(pbcor_image)
        sl_full_df = source_finder.run()
        sources_full[freq] = sl_full_df

        # (Optional) Write source list DataFrame to disk
        write_df_to_disk(sl_full_df, full_source_df_path(freq))

        # Remove temp files:
        source_finder.reset()
    print("Source finding complete in {:.2f}s".format(time() - time_0))
