{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# SKAO SDC1 Workflow on DLaaS: Source classification\n",
    "\n",
    "This notebook takes the output sources produced by the source finding step, and builds a model, based on the training image and source-of-truth catalogue available for this region, using a random forest classifier.\n",
    "\n",
    "This model is then used to classify unseen sources in the full image.\n",
    "\n",
    "Finally, we evaluate the success of the routine using the ska-sdc scoring package and truth catalogue, published after the challenge ended.\n",
    "\n",
    "## Definitions"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "import os\n",
    "from pathlib import Path\n",
    "from time import time\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from ska_sdc import Sdc1Scorer\n",
    "from sklearn.ensemble import RandomForestClassifier\n",
    "from sklearn.metrics import classification_report\n",
    "\n",
    "from ska.sdc1.models.sdc1_image import Sdc1Image\n",
    "from ska.sdc1.utils.bdsf_utils import cat_df_from_srl_df, load_truth_df\n",
    "from ska.sdc1.utils.classification import SKLClassification\n",
    "from ska.sdc1.utils.source_finder import SourceFinder\n",
    "\n",
    "# Challenge frequency bands\n",
    "#\n",
    "FREQS = [560]#, 1400, 9200]\n",
    "\n",
    "def image_path(freq):\n",
    "    return os.path.join(\"data\", \"sample_images\", \"{}mhz_1000h.fits\".format(freq))\n",
    "\n",
    "def train_truth_path(freq):\n",
    "    return os.path.join(\"data\", \"truth\", \"{}mhz_truth_train.txt\".format(freq))\n",
    "\n",
    "\n",
    "def full_truth_path(freq):\n",
    "    return os.path.join(\"data\", \"truth\", \"{}mhz_truth_full.txt\".format(freq))\n",
    "\n",
    "# Output data paths\n",
    "#\n",
    "def train_source_df_path(freq):\n",
    "    return os.path.join(\"data\", \"sources\", \"{}mhz_sources_train.csv\".format(freq))\n",
    "\n",
    "\n",
    "def full_source_df_path(freq):\n",
    "    return os.path.join(\"data\", \"sources\", \"{}mhz_sources_full.csv\".format(freq))\n",
    "\n",
    "\n",
    "def submission_df_path(freq):\n",
    "    return os.path.join(\"data\", \"sources\", \"{}mhz_submission.csv\".format(freq))\n",
    "\n",
    "\n",
    "def model_path(freq):\n",
    "    return os.path.join(\"data\", \"models\", \"{}mhz_classifier.pickle\".format(freq))\n",
    "\n",
    "\n",
    "def score_report_path(freq):\n",
    "    return os.path.join(\"data\", \"score\", \"{}mhz_score.txt\".format(freq))\n",
    "\n",
    "\n",
    "def write_df_to_disk(df, out_path):\n",
    "    \"\"\" Helper function to write DataFrame df to a file at out_path\"\"\"\n",
    "    out_dir = os.path.dirname(out_path)\n",
    "    Path(out_dir).mkdir(parents=True, exist_ok=True)\n",
    "    df.to_csv(out_path, index=False)\n",
    "\n"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Load source catalogues from disk and train classifiers"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "sources_training = {}\n",
    "sources_full = {}\n",
    "for freq in FREQS:\n",
    "    sources_training[freq] = pd.read_csv(train_source_df_path(freq))\n",
    "    sources_full[freq] = pd.read_csv(full_source_df_path(freq))\n",
    "\n",
    "print(\"Training classifiers\")\n",
    "time_0 = time()\n",
    "classifiers = {}\n",
    "for freq, source_train_df in sources_training.items():\n",
    "    # Load truth catalogue for the training area into memory\n",
    "    train_truth_cat_df = load_truth_df(train_truth_path(freq), skiprows=18)\n",
    "\n",
    "    # Construct and train classifier\n",
    "    classifier = SKLClassification(\n",
    "        algorithm=RandomForestClassifier,\n",
    "        classifier_kwargs={\"n_estimators\": 100, \"class_weight\": \"balanced\"},\n",
    "    )\n",
    "    srl_df = classifier.train(\n",
    "        source_train_df, train_truth_cat_df, regressand_col=\"class_t\", freq=freq\n",
    "    )\n",
    "\n",
    "    # Store model for prediction later\n",
    "    classifiers[freq] = classifier\n",
    "\n",
    "    # (Optional) Write model to disk; allows later loading without retraining.\n",
    "    model_dir = os.path.dirname(model_path(freq))\n",
    "    Path(model_dir).mkdir(parents=True, exist_ok=True)\n",
    "    classifier.save_model(model_path(freq))\n",
    "print(\"Model training complete in {:.2f}s\".format(time() - time_0))"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Source classification"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "print(\"Classifying...\")\n",
    "time_0 = time()\n",
    "for freq, source_df in sources_full.items():\n",
    "    source_df[\"class\"] = classifiers[freq].test(source_df)\n",
    "    class_prob = classifiers[freq].predict_proba(source_df)\n",
    "    source_df[\"class_prob\"] = np.amax(class_prob, axis=1)\n",
    "\n",
    "    # (Optional) Write source list DataFrame to disk\n",
    "    write_df_to_disk(source_df, full_source_df_path(freq))\n",
    "print(\"Classification complete in {:.2f}s\".format(time() - time_0))"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "## Create final catalogues and calculate scores"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# 6) Create final catalogues and calculate scores\n",
    "print(\"Calculating final score...\")\n",
    "time_0 = time()\n",
    "for freq, source_df in sources_full.items():\n",
    "    # Assemble submission and truth catalogues for scoring\n",
    "    sub_cat_df = cat_df_from_srl_df(source_df, guess_class=False)\n",
    "    truth_cat_df = load_truth_df(full_truth_path(freq), skiprows=0)\n",
    "\n",
    "    # (Optional) Write final submission catalogue to disk\n",
    "    write_df_to_disk(sub_cat_df, submission_df_path(freq))\n",
    "\n",
    "    # Calculate score\n",
    "    scorer = Sdc1Scorer(sub_cat_df, truth_cat_df, freq)\n",
    "    score = scorer.run(mode=0, train=False, detail=True)\n",
    "\n",
    "    # Write short score report:\n",
    "    score_path = score_report_path(freq)\n",
    "    score_dir = os.path.dirname(score_path)\n",
    "    Path(score_dir).mkdir(parents=True, exist_ok=True)\n",
    "\n",
    "    with open(score_path, \"w+\") as report:\n",
    "        report.write(\n",
    "            \"Image: {}, frequency: {} MHz\\n\".format(image_path(freq), freq)\n",
    "        )\n",
    "        report.write(\"Score was {}\\n\".format(score.value))\n",
    "        report.write(\"Number of detections {}\\n\".format(score.n_det))\n",
    "        report.write(\"Number of matches {}\\n\".format(score.n_match))\n",
    "        report.write(\n",
    "            \"Number of matches too far from truth {}\\n\".format(score.n_bad)\n",
    "        )\n",
    "        report.write(\"Number of false detections {}\\n\".format(score.n_false))\n",
    "        report.write(\"Score for all matches {}\\n\".format(score.score_det))\n",
    "        report.write(\"Accuracy percentage {}\\n\".format(score.acc_pc))\n",
    "        report.write(\"Classification report: \\n\")\n",
    "        report.write(\n",
    "            classification_report(\n",
    "                score.match_df[\"class_t\"],\n",
    "                score.match_df[\"class\"],\n",
    "                labels=[1, 2, 3],\n",
    "                target_names=[\"1 (SS-AGN)\", \"2 (FS-AGN)\", \"3 (SFG)\"],\n",
    "                digits=4,\n",
    "            )\n",
    "        )\n",
    "\n",
    "print(\"Complete in {:.2f}s\".format(time() - time_0))"
   ],
   "outputs": [],
   "metadata": {}
  }
 ],
 "metadata": {
  "orig_nbformat": 4,
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}